# Extra Credit Emulator ECEN 160
A microprocessor emulator for ECEN 160 Final Project
by Steven P. Davies.  
 > Updated March 2018 by Asa Skousen for Randy Jack's
 > three ported register file version of the final project.   
 > Updated by Asa Skousen for Ron Jones 

### Updated by Hunter Wilhelm to help with Extra Credit (07/2020)
  - Added JUMP
  - Added MULT
  - I made it easy to change the amount of registers (find registersCount to change it)
  - Added 8 bit functionality (now 28 bits in total)
  - Added the ability to have comments
  - Added copiable machine code
  - Added button to copy said machine code
